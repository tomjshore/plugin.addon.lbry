
import sys
from src.services import lbrynet
from src.controllers.router import route
from src.views.video import search_videos 


p_open = None
try:
    p_open = lbrynet.start()
    route(sys.argv[2])
finally:
    if not p_open == None:
        p_open.kill()
