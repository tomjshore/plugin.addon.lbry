import requests
import xbmc
from src.models.stream_request import StreamRequest
from src.services.lbrynet import lbry_server_address


def play_video(permanent_url):
    """
    Plays a video from the permanent url
    :param permanent_url: The url of the video minus the protocol of lbry://
    """
    stream_response = requests.post(lbry_server_address,
                                    json=StreamRequest(permanent_url))
    stream = stream_response.json()
    video_url = stream['result']['streaming_url']
    xbmc.log('Playing video ' + video_url, xbmc.LOGINFO)
    xbmc.Player().play(video_url)
