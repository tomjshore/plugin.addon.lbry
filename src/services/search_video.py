import requests
from src.models.search_request import SearchRequest
import xbmc
from src.services.lbrynet import lbry_server_address

sql_time_out_code = -32500


def search(search_term):
    """
    Search for a video on lbry, this is a free text search for videos only
    :param search_term: the text to search with
    :return: a list of claims based on https://lbry.tech/api/sdk#claim_search
    or a RuntimeWarning if the query times out
    """
    xbmc.log('Searching for ' + search_term, xbmc.LOGINFO)
    request = SearchRequest(search_term)
    response = requests.post(lbry_server_address, json=request)
    if response.status_code == 200:
        xbmc.log('Got response', xbmc.LOGINFO)
        xbmc.log(str(response), xbmc.LOGINFO)
        lbry_response = response.json()

        if 'error' in lbry_response \
                and lbry_response['error']['code'] == sql_time_out_code:
            raise RuntimeWarning('Query is too generic please try again')

        xbmc.log(str(lbry_response), xbmc.LOGINFO)
        return lbry_response['result']['items']
