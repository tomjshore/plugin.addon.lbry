import subprocess
import xbmc
import os
import stat


lbry_server_address = 'http://localhost:5279'


def start():
    """Starts the lbrynet server in a subprocess"""
    path = xbmc.translatePath(
            'special://home/addons/plugin.video.lbry/lib/lbrynet')
    __set_permissions__(path)
    xbmc.log('Running command {} {}'.format(path, 'start'), xbmc.LOGNOTICE)
    return subprocess.Popen([path, 'start'])


def __set_permissions__(path):
    """Check if users has permission to execute lbrynet
    if they don't it changes the permissions so they do"""
    if not bool(os.stat(path).st_mode & stat.S_IXOTH):
        os.chmod(path, 0o544)
