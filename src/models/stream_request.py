class StreamRequest(dict):
    """ A model used to get a stream via a http / requests
    See https://lbry.tech/api/sdk#get
    """

    def __init__(self, claim_id):
        super(StreamRequest, self).__init__()
        self['method'] = 'get'
        self['params'] = {
            'uri': claim_id
        }
