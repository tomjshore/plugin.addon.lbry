class SearchRequest(dict):
    """ A model used to search for videos via a http / requests
    See https://lbry.tech/api/sdk#claim_search
    """

    def __init__(self, search_term):
        super(SearchRequest, self).__init__()
        self['method'] = 'claim_search'
        self['params'] = {
            'text': search_term,
            'stream_types': ['video'],
            'media_types': ['video/mp4'],
            'no_totals': True
        }
