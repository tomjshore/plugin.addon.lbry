import sys
from urlparse import parse_qsl
from src.services.player import play_video
from src.views.video import search_videos


def route(query):
    params = dict(parse_qsl(query[1:]))

    if params and params['action'] == 'play':
        play_video(params['video_url'])
    else:
        search_videos(int(sys.argv[1]))
