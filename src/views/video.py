import xbmcgui
import xbmcplugin
import sys
import xbmc
from src.services import search_video as search_service


def display_videos(handle, videos):
    """
    Makes a list of ListItem for each video and displays them
    :param handle: integer - handle the plugin was started with
    :param videos: a List of videos
    """
    items = []
    for video in videos:
        metadata = video['value']

        title = ''
        description = ''
        thumbnail = None
        duration = -1

        if 'title' in metadata:
            title = metadata['title']

        if 'description' in metadata:
            description = metadata['description']

        if 'thumbnail' in metadata:
            thumbnail = metadata['thumbnail']['url']

        if 'video' in metadata and 'duration' in metadata['video']:
            duration = metadata['video']['duration']

        item = xbmcgui.ListItem(label=title, thumbnailImage=thumbnail)
        item.setInfo('video', {'plot': description, 'duration': duration})
        url = '{0}?action=play&video_url={1}'\
            .format(sys.argv[0], video['permanent_url'][7:])
        items.append((url, item, False))

    xbmcplugin.addDirectoryItems(handle, items, len(items))
    xbmcplugin.endOfDirectory(handle)


def search_videos(handle):
    """
    Asks the user for a search term the searches for it
    :param handle: integer - handle the plugin was started with
    """
    dialog = xbmcgui.Dialog()
    search_term = dialog.input('Search for Video')
    xbmc.log('User has search for ' + search_term, xbmc.LOGINFO)
    try:
        videos = search_service.search(search_term)
        display_videos(handle, videos)
    except RuntimeWarning:
        xbmcgui.Dialog().textviewer('Error', str(sys.exc_info()[1]), True)
        search_videos(handle)
