video_meta_data = {
    "address": "mwfK8kVb2w4P5HMQG7qF9FJUphvwvv276R",
    "amount": "1.0",
    "canonical_url": "lbry://@channel#3/astream#b",
    "claim_id": "b417290577f86e33f746980cc43dcf35a76e3374",
    "claim_op": "update",
    "confirmations": 1,
    "height": 214,
    "is_channel_signature_valid": True,
    "meta": {},
    "name": "astream",
    "normalized_name": "astream",
    "nout": 0,
    "permanent_url": "lbry:/    /astream#b417290577f86e33f746980cc43dcf35a76e3374",
    "short_url": "lbry://astream#b",
    "signing_channel": {},
    "timestamp": "1466680974",
    "txid": "c33ac1b97d0c646cd2fb9af4932adfc9c0cb224073933415ccb4d5135e2bb047",
    "type": "claim",
    "value": {
      "source": {
        "hash": "fdbd8e75a67f29f701a4e040385e2e23986303ea10239211af907fcbb83578b3e417cb71ce646efd0819dd8c088de1bd",
        "media_type": "application/octet-stream",
        "name": "tmp4s2rv5lr",
        "sd_hash": "63004f5578b24360b3c67cffa552d07a4a334375afb7972c8beb47b835a1cec86b7208dc17addf852377e6afb78b8b81",
        "size": "11"
      },
      "title": "astream",
      "stream_type": "binary"
    },
    "value_type": "stream"
}


search_video_response = {
    "jsonrpc": "2.0",
    "result": {
        "blocked": {
            "channels": [],
            "total": 0
        },
        "items": [ video_meta_data ],
        "page": 1,
        "page_size": 20,
        "total_items": 1,
        "total_pages": 1
    }
}

search_sql_timeout_response = {
    "jsonrpc": "2.0",
    "error": {
        "message": "sqlite query timed out",
        "code": -32500
    }
}