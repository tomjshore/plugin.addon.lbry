from os import path
import pycodestyle
import unittest


class TestStyle(unittest.TestCase):
    """
    unit test to check that all py files in the project follow
    pep 8 or what it is now all pycodestyle
    """

    def test_check_styles(self):
        found_files = []
        path.walk('.', find_py_files, found_files)
        style = pycodestyle.StyleGuide(quite=False)
        result = style.check_files(found_files)
        self.assertEqual(result.total_errors, 0)


def find_py_files(found_files, directory, sub_files):
    """
    finds all python files in the project
    """
    if './tests' == directory[:6] or './src' == directory[:5]:
        for py_file in sub_files:
            length = len(py_file)
            if length > 2 and '.py' == py_file[length - 3:]:
                found_files.append('{}/{}'.format(directory, py_file))
