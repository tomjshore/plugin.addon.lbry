import unittest
import xbmcgui
import xbmcplugin
from mockito import unstub, mock, when, verify, ANY
from tests.mocks.video import video_meta_data 
from src.views.video import display_videos, search_videos
from src.services import search_video as search_service


class TestVideo(unittest.TestCase):

    def setUp(self):
        self.mock_dialog = mock()
        self.mock_video = mock()
        when(xbmcplugin).addDirectoryItems(ANY, ANY, ANY)
        when(xbmcplugin).endOfDirectory(ANY)
        when(xbmcgui).ListItem(thumbnailImage=None, label='astream').thenReturn(self.mock_video)
        when(xbmcgui).Dialog().thenReturn(self.mock_dialog)
        self.video_url = '/usr/bin/python2 -m nose?action=play&video_url=   /astream#b417290577f86e33f746980cc43dcf35a76e3374'

    def tearDown(self):
        unstub()

    def test_display_videos_makes_kodi_directory(self):
        """
        When videos are passed in then they are converted to kodi items 
        and put in the directory
        """
        # given
        handle = 9
        videos = [video_meta_data, video_meta_data]

        # when
        display_videos(handle, videos)

        # then
        expected_videos = [ (self.video_url, self.mock_video, False),
                (self.video_url, self.mock_video, False) ]
        verify(xbmcplugin).addDirectoryItems(handle, expected_videos, 2)
        verify(xbmcplugin).endOfDirectory(handle)

    def test_search_calls_service_search_when_user_inputs(self):
        # given
        handle = 10
        when(self.mock_dialog).input('Search for Video').thenReturn('cats')
        videos = [video_meta_data, video_meta_data]
        when(search_service).search('cats').thenReturn(videos)

        # when
        search_videos(handle)

        # then
        expected_videos = [ (self.video_url, self.mock_video, False),
                (self.video_url, self.mock_video, False) ]
        verify(xbmcplugin).addDirectoryItems(handle, expected_videos, 2)
        verify(xbmcplugin).endOfDirectory(handle)

    def test_search_show_message_box_if_warning_raised(self):
        # given
        handle = 10
        when(self.mock_dialog).input('Search for Video').thenReturn('cats')
        videos = [video_meta_data, video_meta_data]
        when(search_service).search('cats').thenRaise(RuntimeWarning('error')).thenReturn(videos)

        # when
        search_videos(handle)

        # then
        verify(self.mock_dialog).textviewer('Error', 'error', True)
