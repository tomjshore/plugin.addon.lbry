import unittest
from mockito import unstub, when, mock
import requests
from src.services.search_video import search
from src.models.search_request import SearchRequest
from tests.mocks.video import video_meta_data, search_video_response, search_sql_timeout_response


class TestVideoSearch(unittest.TestCase):

    def test_lbrynet_retunrs_videos_when_given_search_term_hp_laptops(self):
        """When the user enters the search term 'hp laptops' lbrynet is pings and
        returns a list of videos"""
        # given
        search_term = 'hp laptops'
        response = mock({'status_code': 200})
        when(response).json().thenReturn(search_video_response)

        when(requests).post('http://localhost:5279',
                json=SearchRequest('hp laptops')).thenReturn(response)
        # when
        videos = search(search_term)

        # then
        self.assertEqual(1, len(videos))
        self.assertEqual(video_meta_data, videos[0])
        unstub()

    def test_lbrynet_returns_bad_thing_when_sql_timeout(self):
        """When we get an sqltimeout from lbrynet return an exception
       """
        # given
        search_term = 'cats'
        response = mock({'status_code': 200})
        when(response).json().thenReturn(search_sql_timeout_response)

        when(requests).post('http://localhost:5279',
                            json=SearchRequest('cats')).thenReturn(response)
        # when
        error_raised = False
        try:
            videos = search(search_term)
        except RuntimeWarning:
            error_raised = True

        # then
        self.assertTrue(error_raised)
        unstub()