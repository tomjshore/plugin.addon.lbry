import unittest
import subprocess
import os
import xbmc
from mockito import when, unstub, mock, verify
from src.services import lbrynet


class TestLbryNet(unittest.TestCase):

    def tearDown(self):
        unstub()

    def test_subprocess_start(self):
        """When start is called a subprocess is made of 'lbrynet start'"""
        # given
        p_open = mock({'pid': 7})
        special_path = 'special://home/addons/plugin.video.lbry/lib/lbrynet'
        app_path = '~/lbrynet'
        when(subprocess).Popen([app_path, 'start']).thenReturn(p_open)
        when(xbmc).translatePath(special_path).thenReturn(app_path)

        execute_mod = mock({'st_mode': 33063})
        when(os).stat(app_path).thenReturn(execute_mod)

        # when
        result = lbrynet.start()

        # then
        verify(subprocess).Popen([app_path, 'start'])
        self.assertEqual(result, p_open)

    def test_changes_permission_if_wrong(self):
        """chages to execute permission to true if its not"""
        # given
        p_open = mock({'pid': 7})
        special_path = 'special://home/addons/plugin.video.lbry/lib/lbrynet'
        app_path = '~/lbrynet'
        when(subprocess).Popen([app_path, 'start']).thenReturn(p_open)
        when(xbmc).translatePath(special_path).thenReturn(app_path)

        execute_mod = mock({'st_mode': 00000})
        when(os).stat(app_path).thenReturn(execute_mod)
        when(os).chmod(app_path, 0o544).thenReturn(None)

        # when
        lbrynet.start()

        # then
        verify(os).stat(app_path)
        verify(os).chmod(app_path, 0o544)
