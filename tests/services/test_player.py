import unittest
from src.services.player import play_video
from src.models.stream_request import StreamRequest
from mockito import verify, unstub, when, mock
import xbmc
import requests
from tests.mocks.stream import stream


class TestPlayerService(unittest.TestCase):

    def setUp(self):
        self.player = mock()
        when(xbmc).Player().thenReturn(self.player)

    def test_plays_video(self):
        """Test a video plays when given a claim id"""
        # given
        stream_response = mock()
        claim_id = 'abc'
        when(requests).post('http://localhost:5279', json=StreamRequest(claim_id)).thenReturn(stream_response)
        when(stream_response).json().thenReturn(stream)

        # when
        play_video(claim_id)

        # then
        verify(self.player).play('http://localhost:5280/stream/abcd')
        unstub()
