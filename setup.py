from setuptools import setup

setup(
    name = "addon.video.lbry",
    version = "0.0.1",
    author = "tomjshore",
    author_email = "tom@tomjshore.co.uk",
    packages=['src', 'tests'],
    install_requires=[
          'nose', 'mockito', 'pycodestyle', 'Kodistubs', 'requests'
    ]
)
